ARCHIVED
========

This project has been archived and will not be updated anymore, it has been integrated in the Miraverse project. It only remain here for historic purpose.

Drawing
=======

About
-----

2D Drawing using SFML

Build
-----

- Install [SFML 2.5.1](https://www.sfml-dev.org/download/sfml/2.5.1/)
- Clone using the `--recursive` flag to grab the dependencies

```
mkdir build
cd build
cmake ..
make 
./Drawing
```

License
-------

All Drawing source code and assets are provided under the MIT license (See LICENSE file)
