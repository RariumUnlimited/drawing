// Copyright RariumUnlimited - Licence : MIT
#include "Window/Window.h"

int main(int argc, char* argv[]) {
    Drawing::Window window;

    window.Run();
}

