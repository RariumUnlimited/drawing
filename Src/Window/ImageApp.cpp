// Copyright RariumUnlimited - Licence : MIT
#include "Window/ImageApp.h"

namespace Drawing {

ImageApp::ImageApp(const sf::Vector2u& size) {
    image.create(size.x, size.y, sf::Color::Black);
    texture.loadFromImage(image);
    sprite.setTexture(texture);

    buffer = image;
}

void ImageApp::Update() {
    UpdateBuffer();
    ImageMTX.lock();
    image = buffer;
    ImageMTX.unlock();
}

void ImageApp::Draw(sf::RenderTarget &target) {
    ImageMTX.lock();
    int i = 0;
    if (i == 1)
        image.saveToFile("Temp.png");
    texture.update(image);
    target.draw(sprite);
    ImageMTX.unlock();
}

}  // namespace Drawing
