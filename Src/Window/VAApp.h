// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_WINDOW_VAAPP_H_

#define SRC_WINDOW_VAAPP_H_

#include <atomic>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <SFML/Graphics.hpp>

#include "Window/App.h"

namespace Drawing {

/**
 * @brief An base app that draw using a vertex array
 */
class VAApp : public App {
 public:
    /**
     * @brief Construct an app
     */
    VAApp() { }

    /// Update the image
    void Update() final;

    /// Draw the image
    void Draw(sf::RenderTarget &target) final;

    /// Update the image buffer
    virtual void UpdateBuffer() = 0;

 protected:
    sf::VertexArray buffer;  ///< Buffer that will need to be updated with shape to draw

 private:
    std::mutex VAMTX;  ///< Buffer mutex

    sf::VertexArray va;  ///< vertex array that will be display
};

}  // namespace Drawing

#endif  // SRC_WINDOW_VAAPP_H_
