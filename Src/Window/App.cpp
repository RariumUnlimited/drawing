// Copyright RariumUnlimited - Licence : MIT
#include "Window/App.h"

namespace Drawing {

void App::Stop() {
    running = false;
    thread->join();
}

void App::Launch() {
    Reset();
    running = true;
    thread = std::unique_ptr<std::thread>(new std::thread(&App::Run, this));
}

void App::Run() {
    while (running) {
        Update();
    }
}

}  // namespace Drawing
