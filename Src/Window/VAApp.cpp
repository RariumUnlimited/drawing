// Copyright RariumUnlimited - Licence : MIT
#include "Window/VAApp.h"

#include <iostream>

namespace Drawing {

void VAApp::Update() {
    UpdateBuffer();
    VAMTX.lock();
    va.setPrimitiveType(buffer.getPrimitiveType());
    va.clear();
    for (unsigned int i = 0; i < buffer.getVertexCount(); ++i) {
        va.append(buffer[i]);
    }
    VAMTX.unlock();
}

void VAApp::Draw(sf::RenderTarget &target) {
    VAMTX.lock();
    target.draw(va);
    VAMTX.unlock();
}

}  // namespace Drawing
