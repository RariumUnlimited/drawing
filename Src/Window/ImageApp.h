// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_WINDOW_IMAGEAPP_H_

#define SRC_WINDOW_IMAGEAPP_H_

#include <atomic>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <SFML/Graphics.hpp>

#include "Window/App.h"

namespace Drawing {

/**
 * @brief An app that draw in an image
 */
class ImageApp : public App {
 public:
    /**
     * @brief Construct an app
     */
    explicit ImageApp(const sf::Vector2u& size);

    /// Update the image
    void Update() final;

    /// Draw the image
    void Draw(sf::RenderTarget &target) final;

    /// Update the image buffer
    virtual void UpdateBuffer() = 0;


 protected:
    sf::Image buffer;  ///< Image where we will draw things

 private:
    std::mutex ImageMTX;  ///< Image mutex

    sf::Image image;  ///< Image that will be display
    sf::Texture texture;  ///< Texture that we will use to draw on screen
    sf::Sprite sprite;  ///< Sprite that will be drawn
};

}  // namespace Drawing

#endif  // SRC_WINDOW_IMAGEAPP_H_
