// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_WINDOW_WINDOW_H_

#define SRC_WINDOW_WINDOW_H_

#include <string>
#include <unordered_map>

#include <Rarium/Window/Window.h>
#include <SFML/Graphics.hpp>

#include "Window/App.h"

namespace Drawing {

/**
 * @brief A window where we will this our game
 */
class Window : public RR::Window {
 public:
    /**
     * @brief Construct and open a new window
     */
    Window();
    /**
     * @brief Window destructor, delete the app
     */
    ~Window();

    /**
     * @brief Draw the app
     */
    void Draw(float delta, sf::RenderTarget& target) override;

    static const sf::Vector2u Size;  ///< Size of the window (in pixels)
    static sf::Font Font;  ///< Font used to draw text accross all uis

 protected:
    void ChangeApp(const RR::InputState& is, sf::Keyboard::Key key);
    std::string GetText();
    void ResetKeyEventMap();

    inline void SwitchText(const RR::InputState& is, sf::Keyboard::Key key) {
        drawText = !drawText;
    }

    App* app;  ///< App used to draw things
    sf::Text text;  ///< Help text to draw
    sf::Text infoText;  ///< Info text to draw
    bool drawText;  ///< If we draw text
};

}  // namespace Drawing

#endif  // SRC_WINDOW_WINDOW_H_
