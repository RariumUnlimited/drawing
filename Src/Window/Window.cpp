// Copyright RariumUnlimited - Licence : MIT
#include "Window/Window.h"

#include <chrono>
#include <ctime>
#include <iostream>
#include <string>

#include "App/Voronoi.h"
#include "App/Mandelbrot.h"
#include "App/Julia.h"
#include "App/ColorMapViewer.h"
#include "App/VanKoch.h"
#include "App/MultiTable.h"
#include "App/Sierpinski.h"
#include "Tools/Font.h"

namespace Drawing {

Window::Window() :
    RR::Window("Drawing",
               sf::VideoMode(Size.x, Size.y)) {
    ResetKeyEventMap();

    app = new Voronoi(Size, this, 10);

    text.setString(app->GetCommandText() + "\n" + GetText());
    text.setFont(Font);
    text.setCharacterSize(11);
    text.setFillColor(sf::Color::White);


    infoText.setFont(Font);
    infoText.setCharacterSize(11);
    infoText.setFillColor(sf::Color::White);
    infoText.setPosition(Size.x, 0.0);

    drawText = true;

    Font.loadFromMemory(GetFontData(), GetFontSize());
    app->Launch();
}

Window::~Window() {
    app->Stop();
    delete app;
}

void Window::Draw(float delta, sf::RenderTarget& target) {
    app->Draw(target);

    if (drawText) {
        target.draw(text);
        infoText.setString(app->GetInfoText());
        infoText.setOrigin(infoText.getLocalBounds().width + 10, 0.0);
        target.draw(infoText);
    }
}


void Window::ChangeApp(const RR::InputState& is, sf::Keyboard::Key key) {
    app->Stop();
    delete app;

    ResetKeyEventMap();

    if (key == sf::Keyboard::Num1)
        app = new Voronoi(Size, this, 10, true);
    else if (key == sf::Keyboard::Num2)
        app = new Mandelbrot(Size, this);
    else if (key == sf::Keyboard::Num3)
        app = new Julia(Size, this);
    else if (key == sf::Keyboard::Num4)
        app = new ColorMapViewer(Size, this);
    else if (key == sf::Keyboard::Num5)
        app = new VanKoch(Size, this);
    else if (key == sf::Keyboard::Num6)
        app = new MultiTable(Size, this);
    else if (key == sf::Keyboard::Num7)
        app = new Sierpinski(Size, this);
    app->Launch();
    text.setString(app->GetCommandText() + "\n" + GetText());
}

std::string Window::GetText() {
    std::string result = "H : Show/Hide text \n";
    result += "P : Take screenshot \n";
    result += "1 : Voronoi app \n";
    result += "2 : Mandelbrot app \n";
    result += "3 : Julia app \n";
    result += "4 : Color Map Viewer \n";
    result += "5 : VanKoch app \n";
    result += "6 : MultiTable app \n";
    result += "7 : Sierpinski app";
    return result;
}

void Window::ResetKeyEventMap() {
    KeyPressedEvents.clear();
    MouseButtonDoubleClickEvent.Clear();

    KeyPressedEvents[sf::Keyboard::Key::H]
            += new RR::KeyEvent::T<Window>(this, &Window::SwitchText);
    KeyPressedEvents[sf::Keyboard::Key::Num1]
            += new RR::KeyEvent::T<Window>(this, &Window::ChangeApp);
    KeyPressedEvents[sf::Keyboard::Key::Num2]
            += new RR::KeyEvent::T<Window>(this, &Window::ChangeApp);
    KeyPressedEvents[sf::Keyboard::Key::Num3]
            += new RR::KeyEvent::T<Window>(this, &Window::ChangeApp);
    KeyPressedEvents[sf::Keyboard::Key::Num4]
            += new RR::KeyEvent::T<Window>(this, &Window::ChangeApp);
    KeyPressedEvents[sf::Keyboard::Key::Num5]
            += new RR::KeyEvent::T<Window>(this, &Window::ChangeApp);
    KeyPressedEvents[sf::Keyboard::Key::Num6]
            += new RR::KeyEvent::T<Window>(this, &Window::ChangeApp);
    KeyPressedEvents[sf::Keyboard::Key::Num7]
            += new RR::KeyEvent::T<Window>(this, &Window::ChangeApp);
}

const sf::Vector2u Window::Size = sf::Vector2u(640, 480);
sf::Font Window::Font;

}  // namespace Drawing
