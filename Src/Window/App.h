// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_WINDOW_APP_H_

#define SRC_WINDOW_APP_H_

#include <atomic>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <SFML/Graphics.hpp>

namespace Drawing {

/**
 * @brief An base app that draw in a texture
 */
class App {
 public:
    /// Launch the thread
    void Launch();

    /// Stop the thread
    void Stop();

    /// Thread loop
    void Run();

    virtual void Reset() { }

    /// Update the app
    virtual void Update() = 0;

    /// Draw the app
    virtual void Draw(sf::RenderTarget &target) = 0;

    virtual std::string GetCommandText() {
        return "";
    }

    virtual std::string GetInfoText() {
        return "";
    }

 private:
    std::atomic_bool running;  ///< If the thread is running
    std::unique_ptr<std::thread> thread;  ///< Thread that will update the app
};

}  // namespace Drawing

#endif  // SRC_WINDOW_APP_H_
