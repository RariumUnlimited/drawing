// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_TOOLS_COLORMAP_H_

#define SRC_TOOLS_COLORMAP_H_

#include <map>
#include <utility>

#include <SFML/Graphics.hpp>

namespace Drawing {

/**
 * @brief An base app that draw in a texture
 */
class ColorMap {
 public:
    ColorMap() {}
    explicit ColorMap(const std::map<double, sf::Color>& cM);

    inline const std::map<double, sf::Color>& GetColorMap() const {
        return colorMap;
    }

    inline std::map<double, sf::Color>& GetColorMap() {
        return colorMap;
    }

    sf::Color GetColor(double f) const;

 protected:
    std::map<double, sf::Color> colorMap;

    static sf::Color Lerp(sf::Color a, sf::Color b, float t);
};

}  // namespace Drawing

#endif  // SRC_TOOLS_COLORMAP_H_
