// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_TOOLS_FONT_H_

#define SRC_TOOLS_FONT_H_

namespace Drawing {

const long int GetFontSize();
const unsigned char* GetFontData();

}  // namespace Drawing

#endif  // SRC_TOOLS_FONT_H_
