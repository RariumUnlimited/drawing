// Copyright RariumUnlimited - Licence : MIT
#include "Tools/ColorMap.h"

#include <cassert>
#include <cmath>
#include <iostream>

#include <Rarium/Tools/Math.h>

namespace Drawing {

ColorMap::ColorMap(const std::map<double, sf::Color>& cM) :
    colorMap(cM) {
}

sf::Color ColorMap::GetColor(double f) const {
    assert(colorMap.size() >= 2);

    auto it = colorMap.begin();
    ++it;

    for (; it != colorMap.end(); ++it) {
        if (f < it->first) {
            auto it2 = it;
            --it2;
            sf::Color a = it2->second;
            sf::Color b = it->second;
            double lerpFactor = (f - it2->first) /
                                (it->first - it2->first);
            return Lerp(a, b, lerpFactor);
        }
    }

    return Lerp(colorMap.begin()->second, (++colorMap.begin())->second, f);
}

sf::Color ColorMap::Lerp(sf::Color a, sf::Color b, float t) {
    sf::Color result;
    result.r = a.r + (b.r - a.r) * t;
    result.g = a.g + (b.g - a.g) * t;
    result.b = a.b + (b.b - a.b) * t;
    return result;
}

}  // namespace Drawing
