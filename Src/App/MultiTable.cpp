// Copyright RariumUnlimited - Licence : MIT
#include "App/MultiTable.h"

#include <omp.h>

#include <cfloat>
#include <chrono>
#include <cmath>
#include <iostream>
#include <limits>
#include <utility>

#include "Window/Window.h"

namespace Drawing {

MultiTable::MultiTable(const sf::Vector2u& size, Window* window) {
    width = size.x;
    height = size.y;
    minUpdateRate = 60;
    useMinUpdateRate = true;

    window->KeyPressedEvents[sf::Keyboard::Key::B]
            += new RR::KeyEvent::T<MultiTable>(this, &MultiTable::SwitchUseMinUpdateRate);
    window->KeyPressedEvents[sf::Keyboard::Key::Z]
            += new RR::KeyEvent::T<MultiTable>(this, &MultiTable::IncreaseTable);
    window->KeyPressedEvents[sf::Keyboard::Key::S]
            += new RR::KeyEvent::T<MultiTable>(this, &MultiTable::DecreaseTable);
    window->KeyPressedEvents[sf::Keyboard::Key::R]
            += new RR::KeyEvent::T<MultiTable>(this, &MultiTable::IncreaseMod);
    window->KeyPressedEvents[sf::Keyboard::Key::F]
            += new RR::KeyEvent::T<MultiTable>(this, &MultiTable::DecreaseMod);

    circleVertexCount = 360;
    buffer = sf::VertexArray(sf::Lines);
    float step = 2.f * 3.14f / 180.f;
    center = sf::Vector2f(width / 2.f, height / 2.f);
    r = height / 2.f - 30.f;

    for (int i = 0; i < 180; ++i) {
        float x1 = r * cos(step * i) + center.x;
        float y1 = r * sin(step * i) + center.y;
        float x2 = r * cos(step * (i + 1)) + center.x;
        float y2 = r * sin(step * (i + 1)) + center.y;
        sf::Vertex v1(sf::Vector2f(x1, y1), sf::Color::White);
        sf::Vertex v2(sf::Vector2f(x2, y2), sf::Color::White);
        buffer.append(v1);
        buffer.append(v2);
    }

    table = 2;
    mod = 6;

    cm.GetColorMap().insert(std::make_pair(0.f, sf::Color::Red));
    cm.GetColorMap().insert(std::make_pair(0.2f, sf::Color::Yellow));
    cm.GetColorMap().insert(std::make_pair(0.4f, sf::Color::Cyan));
    cm.GetColorMap().insert(std::make_pair(0.6f, sf::Color::Blue));
    cm.GetColorMap().insert(std::make_pair(0.8f, sf::Color::Magenta));
    cm.GetColorMap().insert(std::make_pair(1.0f, sf::Color::Green));
}

std::string MultiTable::GetCommandText() {
    std::string result = "Command : \n";
    result += "B : Switch min update rate usage \n";
    result += "Z/S : Increase/Decrease table \n";
    result += "R/F : Increase/Decrease mod";
    return result;
}

std::string MultiTable::GetInfoText() {
  std::string result = "Info : \n";
  result += "MinUpdateRate : " + std::to_string(minUpdateRate) +
          " " + (useMinUpdateRate ? "Active" : "Not Active") + "\n";
  result += "Table : " + std::to_string(table) + "\n";
  result += "Mod : " + std::to_string(mod) + "\n";
  result += "I : " + std::to_string(currentI) + "\n";
  return result;
}

void MultiTable::SwitchUseMinUpdateRate(const RR::InputState& is,
                                        sf::Keyboard::Key k) {
    useMinUpdateRate = !useMinUpdateRate;
}

void MultiTable::Reset() {
    resetMtx.lock();
    currentI = 1;
    buffer.resize(circleVertexCount);
    resetMtx.unlock();
}

void MultiTable::UpdateBuffer() {
    resetMtx.lock();
    sf::Clock clock;

    float step = 2.f * 3.14 / mod;

    if (currentI < mod) {
        int nextI = ((currentI * table) % mod);
        float x1 = r * cos(step * currentI) + center.x;
        float y1 = r * sin(step * currentI) + center.y;
        float x2 = r * cos(step * nextI) + center.x;
        float y2 = r * sin(step * nextI) + center.y;
        sf::Vertex v1(sf::Vector2f(x1, y1), cm.GetColor(static_cast<double>(currentI) / static_cast<double>(mod)));
        sf::Vertex v2(sf::Vector2f(x2, y2), cm.GetColor(static_cast<double>(nextI) / static_cast<double>(mod)));
        buffer.append(v1);
        buffer.append(v2);
        ++currentI;
    }

    int timeMS = clock.getElapsedTime().asMilliseconds();
    float time = clock.getElapsedTime().asSeconds();

    std::cout << "MultiTable time : "
              << time << "s"
              << std::endl;

    if (timeMS < minUpdateRate && useMinUpdateRate) {
        std::this_thread::sleep_for(
                    std::chrono::milliseconds(minUpdateRate - timeMS));
    }

    resetMtx.unlock();
}

void MultiTable::IncreaseTable(const RR::InputState& is, sf::Keyboard::Key k) {
    ++table;
    Reset();
}

void MultiTable::DecreaseTable(const RR::InputState& is, sf::Keyboard::Key k) {
    if (table > 2) {
        --table;
        Reset();
    }
}

void MultiTable::IncreaseMod(const RR::InputState& is, sf::Keyboard::Key k) {
    ++mod;
    Reset();
}

void MultiTable::DecreaseMod(const RR::InputState& is, sf::Keyboard::Key k) {
    if (mod > 2) {
        --mod;
        Reset();
    }
}

}  // namespace Drawing
