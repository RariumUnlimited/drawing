// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_APP_JULIA_H_

#define SRC_APP_JULIA_H_

#include <complex>
#include <random>
#include <string>
#include <vector>

#include <Rarium/Window/InputState.h>
#include <SFML/Graphics.hpp>

#include "Tools/ColorMap.h"
#include "Window/ImageApp.h"

namespace Drawing {

class Window;

/**
 * @brief An app that draw of Julia fractal
 */
class Julia : public ImageApp {
 public:
    /**
     * @brief Construct an new Julia app
     */
    Julia(const sf::Vector2u& size, Window* window);

    /// Update the texture
    void UpdateBuffer() override;

    /// Get the help text of the Julia app
    std::string GetCommandText() override;

    /// Get the Info text of the Julia app
    std::string GetInfoText() override;

 protected:
    void VerticalMove(const RR::InputState& is, sf::Keyboard::Key k);
    void HorizontalMove(const RR::InputState& is, sf::Keyboard::Key k);
    void Zoom(const RR::InputState& is, sf::Keyboard::Key k);
    void IncreaseDim(const RR::InputState& is, sf::Keyboard::Key k);
    void DecreaseDim(const RR::InputState& is, sf::Keyboard::Key k);
    void RandomizeC(const RR::InputState& is, sf::Keyboard::Key k);
    void Stop(const RR::InputState& is, sf::Keyboard::Key k);
    void UltraZoom(const RR::InputState& is, sf::Mouse::Button b);
    void IncreaseMSAA(const RR::InputState& is, sf::Keyboard::Key k);
    void DecreaseMSAA(const RR::InputState& is, sf::Keyboard::Key k);
    void SwitchUseMinUpdateRate(const RR::InputState& is, sf::Keyboard::Key k);
    void SwitchUseMaxIteration(const RR::InputState& is, sf::Keyboard::Key k);

    /// Reset the app : set Z to 0, Running to true and Iteration to 0 for each pixel; and maxIterations to 0
    void Reset();

    /// Reset data layer
    void ResetLayer(int layer);

    /// Reset the app with the new msaa value in argument
    /// We need to change msaa after locking resetMtx to avoid data race when updating
    /// So we must call this one and because the mtx in locked in this one and not Reset()
    void Reset(int msaa);

    /// Get the C of a pixel
    virtual std::complex<double> GetC(sf::Vector2u pixel,
                                      sf::Vector2<double> sample);
    /// Get the Z0 of a pixel
    virtual std::complex<double> GetZ0(sf::Vector2u pixel,
                                       sf::Vector2<double> sample);

    inline sf::Vector3i ColorToVector(const sf::Color& color) {
        return sf::Vector3i(color.r,
                            color.g,
                            color.b);
    }

    double rmin;
    double rmax;
    double imin;
    double imax;
    int iteration;
    int width;
    int height;
    int dim;
    ColorMap cm;
    bool stopped;
    int msaa;

    int minUpdateRate;
    bool useMinUpdateRate;
    int maxIteration;
    bool useMaxIteration;

    int indexDisplayed;
    std::vector<std::complex<double>> cList;

    struct PixelData {
        std::complex<double> Z;  ///< Z of the pixel
        std::complex<double> C;  ///< C of the pixel
        bool Running;  ///< True if we must keep running the iteration for this pixel
        int Iteration;  ///< At which iteration we stopped running
    };

    std::mt19937 generator;  ///< A random number generator
    std::uniform_real_distribution<double> distribSample;  ///< Distribution for the point y coordinate

    std::mutex resetMtx;  ///< Mutex used when to avoid data race when resetting
    std::vector<std::vector<PixelData>> data;  ///< z of each pixel, the bool is set to true when
};

}  // namespace Drawing

#endif  // SRC_APP_JULIA_H_
