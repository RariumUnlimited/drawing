// Copyright RariumUnlimited - Licence : MIT
#include "App/ColorMapViewer.h"

#include <omp.h>

#include <chrono>
#include <cfloat>
#include <iostream>
#include <utility>

#include "Window/Window.h"

namespace Drawing {

ColorMapViewer::ColorMapViewer(const sf::Vector2u& size, Window* window) :
    ImageApp(size) {
    cm.GetColorMap().insert(std::make_pair(0.f, sf::Color::Red));
    cm.GetColorMap().insert(std::make_pair(0.2f, sf::Color::Yellow));
    cm.GetColorMap().insert(std::make_pair(0.4f, sf::Color::Cyan));
    cm.GetColorMap().insert(std::make_pair(0.6f, sf::Color::Blue));
    cm.GetColorMap().insert(std::make_pair(0.8f, sf::Color::Magenta));
    cm.GetColorMap().insert(std::make_pair(1.0f, sf::Color::Green));
}


std::string ColorMapViewer::GetCommandText() {
    return "Command :";
}

void ColorMapViewer::UpdateBuffer() {
    sf::Clock clock;

    #pragma omp parallel for
    for (int i = 0; i < buffer.getSize().x * buffer.getSize().y; ++i) {
        // Compute each pixel coord from the index
        unsigned int x = i % buffer.getSize().x;
        unsigned int y = (i - x) / buffer.getSize().x;

        float factor = static_cast<float>(x) /
                       static_cast<float>(buffer.getSize().x);

        sf::Color color = cm.GetColor(factor);

        buffer.setPixel(x, y, color);
    }

    std::cout << "ColorMapViewer time : " << clock.getElapsedTime().asSeconds() << "s"
              << std::endl;
}

}  // namespace Drawing
