// Copyright RariumUnlimited - Licence : MIT
#include "App/Mandelbrot.h"

#include <omp.h>

#include <chrono>
#include <iostream>

#include "Window/Window.h"

namespace Drawing {

Mandelbrot::Mandelbrot(const sf::Vector2u& size, Window* window) :
    Julia(size, window) {
}

std::complex<double> Mandelbrot::GetC(sf::Vector2u pixel,
                                      sf::Vector2<double> sample) {
    return std::complex<double>(rmin + (pixel.x + sample.x) /
                                width * (rmax - rmin),
                                imin + (pixel.y + sample.y) /
                                height * (imax - imin));
}

std::complex<double> Mandelbrot::GetZ0(sf::Vector2u pixel,
                                       sf::Vector2<double> sample) {
    return 0;
}

}  // namespace Drawing
