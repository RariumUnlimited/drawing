// Copyright RariumUnlimited - Licence : MIT
#include "App/Voronoi.h"

#include <omp.h>

#include <chrono>
#include <iostream>
#include <cfloat>

#include "Window/Window.h"

namespace Drawing {

Voronoi::Voronoi(const sf::Vector2u& size, Window* window, int pointCount, bool color) :
    ImageApp(size),
    generator(std::chrono::system_clock::now().time_since_epoch().count()),
    distribX(0, static_cast<float>(size.x - 1)),
    distribY(0, static_cast<float>(size.y - 1)),
    distribSample(0.f, 1.f),
    distribC(0, 255) {
    this->color = color;

    points.resize(pointCount);
    colors.resize(pointCount);

    // Generate random points
    for (sf::Vector2f& p : points) {
        p.x = distribX(generator);
        p.y = distribY(generator);
    }

    // Generate random colors for each point
    for (sf::Color& c : colors) {
        c.r = distribC(generator);
        if (color) {
            c.g = distribC(generator);
            c.b = distribC(generator);
        } else {
            c.g = c.r;
            c.b = c.r;
        }
    }

    drawPoints = true;

    window->KeyPressedEvents[sf::Keyboard::Key::R] +=
            new RR::KeyEvent::T<Voronoi>(this, &Voronoi::RandomizePoints);
    window->KeyPressedEvents[sf::Keyboard::Key::C] +=
            new RR::KeyEvent::T<Voronoi>(this, &Voronoi::RandomizeColors);
    window->KeyPressedEvents[sf::Keyboard::Key::V] +=
            new RR::KeyEvent::T<Voronoi>(this, &Voronoi::SwitchColor);
    window->KeyPressedEvents[sf::Keyboard::Key::T] +=
            new RR::KeyEvent::T<Voronoi>(this, &Voronoi::SwitchPoints);
    window->KeyPressedEvents[sf::Keyboard::Key::Up] +=
            new RR::KeyEvent::T<Voronoi>(this, &Voronoi::AddPoint);
    window->KeyPressedEvents[sf::Keyboard::Key::Down] +=
            new RR::KeyEvent::T<Voronoi>(this, &Voronoi::RemovePoint);

    pixels.resize(size.x * size.y);
}

float Voronoi::DistanceSquared(const sf::Vector2f& a,
                                      const sf::Vector2f& b) {
    return (b.x - a .x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y);
}

std::string Voronoi::GetCommandText() {
    std::string result = "Command : \n";
    result += "R : Randomize points \n";
    result += "C : Randomize colors \n";
    result += "V : Switch color palette \n";
    result += "T : Switch points display \n";
    result += "Up : Add a point \n";
    result += "Down : Remove a point";
    return result;
}

std::string Voronoi::GetInfoText() {
    std::string result = "Info : \n";
    result += "Point count : " + std::to_string(points.size()) + "\n";
    result += "MSAA : " + std::to_string(msaa);
    return result;
}

void Voronoi::RandomizePoints(const RR::InputState& is, sf::Keyboard::Key k) {
    for (sf::Vector2f& p : points) {
        p.x = distribX(generator);
        p.y = distribY(generator);
    }

    Reset();
}

void Voronoi::RandomizeColors(const RR::InputState& is, sf::Keyboard::Key k) {
    for (sf::Color& c : colors) {
        c.r = distribC(generator);
        if (color) {
            c.g = distribC(generator);
            c.b = distribC(generator);
        } else {
            c.g = c.r;
            c.b = c.r;
        }
    }

    Reset();
}

void Voronoi::SwitchColor(const RR::InputState& is, sf::Keyboard::Key k) {
    color = !color;

    RandomizeColors(is, k);
    Reset();
}

void Voronoi::SwitchPoints(const RR::InputState& is, sf::Keyboard::Key k) {
    drawPoints = !drawPoints;
    Reset();
}

void Voronoi::AddPoint(const RR::InputState& is, sf::Keyboard::Key k) {
    points.push_back(sf::Vector2f(distribX(generator), distribY(generator)));

    sf::Color c;
    c.r = distribC(generator);
    if (color) {
        c.g = distribC(generator);
        c.b = distribC(generator);
    } else {
        c.g = c.r;
        c.b = c.r;
    }
    colors.push_back(c);
    Reset();
}

void Voronoi::RemovePoint(const RR::InputState& is, sf::Keyboard::Key k) {
    if (points.size() > 1) {
        points.pop_back();
        colors.pop_back();
    }
    Reset();
}

sf::Color Voronoi::GetSampleColor(const sf::Vector2f& sample) {
    // We consider our first point to be our nearest neighbour
    unsigned int neighbour = 0;
    float neighbourDistance = DistanceSquared(points[0], sample);


    // We check each other point if he is a closer neighbour
    for (unsigned int j = 1; j < points.size(); ++j) {
        float dTemp = DistanceSquared(points[j], sample);
        if (dTemp < neighbourDistance) {
            neighbourDistance = dTemp;
            neighbour = j;
        }
    }

    return colors[neighbour];
}

void Voronoi::Reset() {
    resetMtx.lock();
    msaa = 1;
    #pragma omp parallel for
    for (int i = 0; i < buffer.getSize().x * buffer.getSize().y; ++i) {
        pixels[i] = sf::Vector3i(0, 0, 0);
    }
    resetMtx.unlock();
}

void Voronoi::UpdateBuffer() {
    resetMtx.lock();
    if (msaa < 1024) {
        sf::Clock clock;

        // For each pixel, only one for loop for easier parallelization
        #pragma omp parallel for
        for (int i = 0; i < buffer.getSize().x * buffer.getSize().y; ++i) {
            // Compute each pixel coord from the index
            unsigned int x = i % buffer.getSize().x;
            unsigned int y = (i - x) / buffer.getSize().x;



            float sampleX = distribSample(generator);
            float sampleY = distribSample(generator);

            sf::Vector2f pixel(static_cast<float>(x) + sampleX,
                               static_cast<float>(y) + sampleY);

            pixels[i] += ColorToVector(GetSampleColor(pixel));

            buffer.setPixel(x, y, sf::Color(pixels[i].x / msaa,
                                            pixels[i].y / msaa,
                                            pixels[i].z / msaa));
        }

        if (drawPoints) {
            // For each point we draw a black square of 3 by 3 pixel
            for (sf::Vector2f& p : points) {
                for (unsigned int j = p.y - 1; j <= p.y + 1; ++j)
                    for (unsigned int i = p.x - 1; i <= p.x + 1; ++i)
                        if (i > 0 && j > 0 &&
                                i < buffer.getSize().x && j < buffer.getSize().y)
                            buffer.setPixel(i, j, sf::Color::Black);
            }
        }

        ++msaa;

        std::cout << "Voronoi time : " << clock.getElapsedTime().asSeconds() << "s"
                  << std::endl;
    }
    resetMtx.unlock();
}

}  // namespace Drawing
