// Copyright RariumUnlimited - Licence : MIT
#include "App/Sierpinski.h"

#include <omp.h>

#include <cfloat>
#include <chrono>
#include <cmath>
#include <iostream>
#include <limits>
#include <string>

#include "Window/Window.h"

#define pi 3.14159265359

namespace Drawing {

Sierpinski::Sierpinski(const sf::Vector2u& size, Window* window) :
    generator(std::chrono::system_clock::now().time_since_epoch().count()),
    distrib(0.f, 1.f),
    distribVertex(0, 2) {
    triangle.resize(3);
    triangle[0].x = size.x / 8;
    triangle[0].y = size.y * 7 / 8;

    triangle[1].x = size.x * 7 / 8;
    triangle[1].y = size.y * 7 / 8;

    triangle[2].x = size.x / 2;
    triangle[2].y = size.y / 8;

    a = triangle[1] - triangle[0];
    b = triangle[2] - triangle[0];

    float u1 = distrib(generator);
    float u2 = distrib(generator);

    if (u1 + u2 > 1.f) {
        u1 = 1 - u1;
        u2 = 1 - u2;
    }

    sf::Vector2f w = a * u1 + b * u2;
    currentPosition = w + triangle[0];

    buffer = sf::VertexArray(sf::Points, 1);
    buffer[0] = currentPosition;

    points.push_back(currentPosition);

    window->KeyPressedEvents[sf::Keyboard::Key::Up] +=
            new RR::KeyEvent::T<Sierpinski>(this, &Sierpinski::IncreasePointPerUpdate);
    window->KeyPressedEvents[sf::Keyboard::Key::Down] +=
            new RR::KeyEvent::T<Sierpinski>(this, &Sierpinski::DecreasePointPerUpdate);
}

std::string Sierpinski::GetCommandText() {
    std::string result = "Command : \n";
    result += "Up : Increase point per update \n";
    result += "Down : Decrease point per update \n";
    return result;
}

std::string Sierpinski::GetInfoText() {
  std::string result = "Info : \n";
  result += "Point count : " + std::to_string(points.size()) + "\n";
  result += "Point per update : " + std::to_string(pointPerUpdate) + "\n";
  return result;
}

void Sierpinski::UpdateBuffer() {
    resetMtx.lock();
    sf::Clock clock;

    for (int i = 0; i < pointPerUpdate; ++i) {
        sf::Vector2f vertex = triangle[distribVertex(generator)];

        currentPosition = (currentPosition + vertex) / 2.f;
        points.push_back(currentPosition);
    }

    buffer.resize(points.size());

    for (int i = 0; i < pointPerUpdate; ++i)
        buffer[points.size() - i - 1] = points[points.size() - i - 1];


    float time = clock.getElapsedTime().asSeconds();

    std::cout << "Sierpinski time : "
              << time << "s"
              << std::endl;

    std::this_thread::sleep_for(std::chrono::milliseconds(20));

    resetMtx.unlock();
}

void Sierpinski::IncreasePointPerUpdate(const RR::InputState& is, sf::Keyboard::Key k) {
    ++pointPerUpdate;
}

void Sierpinski::DecreasePointPerUpdate(const RR::InputState& is, sf::Keyboard::Key k) {
    if (pointPerUpdate > 1)
        --pointPerUpdate;
}

void Sierpinski::Reset() {
    resetMtx.lock();
    points.clear();
    buffer.resize(0);
    resetMtx.unlock();
}

}  // namespace Drawing
