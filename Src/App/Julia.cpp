// Copyright RariumUnlimited - Licence : MIT
#include "App/Julia.h"

#include <omp.h>

#include <cfloat>
#include <chrono>
#include <cmath>
#include <iostream>
#include <limits>
#include <utility>

#include <Rarium/Tools/Math.h>

#include "Window/Window.h"

namespace Drawing {

Julia::Julia(const sf::Vector2u& size, Window* window) :
    ImageApp(size),
    generator(std::chrono::system_clock::now().time_since_epoch().count()),
    distribSample(0.f, 1.f) {
    imin = -1.f;
    imax = 1.f;
    width = size.x;
    height = size.y;
    double temp = 3 * width / (height * 1.5);
    temp /= 2;
    rmin = temp * -1;
    rmax = temp;
    iteration = 512;
    dim = 2;
    msaa = 1;
    minUpdateRate = 60;
    maxIteration = 150;
    useMinUpdateRate = false;
    useMaxIteration = false;

    window->KeyPressedEvents[sf::Keyboard::Key::Left]
            += new RR::KeyEvent::T<Julia>(this, &Julia::HorizontalMove);
    window->KeyPressedEvents[sf::Keyboard::Key::Right]
            += new RR::KeyEvent::T<Julia>(this, &Julia::HorizontalMove);
    window->KeyPressedEvents[sf::Keyboard::Key::Up]
            += new RR::KeyEvent::T<Julia>(this, &Julia::VerticalMove);
    window->KeyPressedEvents[sf::Keyboard::Key::Down]
            += new RR::KeyEvent::T<Julia>(this, &Julia::VerticalMove);

    window->KeyPressedEvents[sf::Keyboard::Key::E]
            += new RR::KeyEvent::T<Julia>(this, &Julia::IncreaseMSAA);
    window->KeyPressedEvents[sf::Keyboard::Key::A]
            += new RR::KeyEvent::T<Julia>(this, &Julia::DecreaseMSAA);
    window->KeyPressedEvents[sf::Keyboard::Key::B]
            += new RR::KeyEvent::T<Julia>(this, &Julia::SwitchUseMinUpdateRate);
    window->KeyPressedEvents[sf::Keyboard::Key::N]
            += new RR::KeyEvent::T<Julia>(this, &Julia::SwitchUseMaxIteration);

    window->KeyPressedEvents[sf::Keyboard::Key::Z] +=
            new RR::KeyEvent::T<Julia>(this, &Julia::Zoom);
    window->KeyPressedEvents[sf::Keyboard::Key::S] +=
            new RR::KeyEvent::T<Julia>(this, &Julia::Zoom);
    window->KeyPressedEvents[sf::Keyboard::Key::R] +=
            new RR::KeyEvent::T<Julia>(this, &Julia::IncreaseDim);
    window->KeyPressedEvents[sf::Keyboard::Key::F] +=
            new RR::KeyEvent::T<Julia>(this, &Julia::DecreaseDim);
    window->KeyPressedEvents[sf::Keyboard::Key::C] +=
            new RR::KeyEvent::T<Julia>(this, &Julia::RandomizeC);
    window->KeyPressedEvents[sf::Keyboard::Key::T] +=
            new RR::KeyEvent::T<Julia>(this, &Julia::Stop);
    window->MouseButtonDoubleClickEvent +=
            new RR::MouseButtonEvent::T<Julia>(this, &Julia::UltraZoom);

    cm.GetColorMap().insert(std::make_pair(0.f, sf::Color::Red));
    cm.GetColorMap().insert(std::make_pair(0.2f, sf::Color::Yellow));
    cm.GetColorMap().insert(std::make_pair(0.4f, sf::Color::Cyan));
    cm.GetColorMap().insert(std::make_pair(0.6f, sf::Color::Blue));
    cm.GetColorMap().insert(std::make_pair(0.8f, sf::Color::Magenta));
    cm.GetColorMap().insert(std::make_pair(1.0f, sf::Color::Green));

    indexDisplayed = 0;
    cList.push_back(std::complex<double>(0.285, 0.01));
    cList.push_back(std::complex<double>(0.3, 0.5));
    cList.push_back(std::complex<double>(-1.417022285618, 0.0099534));
    cList.push_back(std::complex<double>(-0.038088, 0.9754633));
    cList.push_back(std::complex<double>(0.285, 0.013));
    cList.push_back(std::complex<double>(-1.476, 0.0));
    cList.push_back(std::complex<double>(-0.4, 0.6));
    cList.push_back(std::complex<double>(-0.8, 0.156));
    cList.push_back(std::complex<double>(-0.123, 0.745));
}

std::string Julia::GetCommandText() {
    std::string result = "Command : \n";
    result += "Arrows : Move \n";
    result += "Z/S : Zoom\n";
    result += "R/F : Increase/Decrease Dimension \n";
    result += "A/E : Increase/Decrease MSAA \n";
    result += "Double Left Click : Zoom to cursor \n";
    result += "Double Right Click : Center to cursor \n";
    result += "C : Change C (Julia only) \n";
    result += "B : Switch min update rate usage \n";
    result += "N : Switch max iteration usage \n";
    result += "T : Stop iteration";
    return result;
}

std::string Julia::GetInfoText() {
  std::string result = "Info : \n";
  result += "Dimension : " + std::to_string(dim) + "\n";
  result += "Iteration : " + std::to_string(iteration) + "\n";
  result += "MSAA : " + std::to_string(msaa) + "\n";
  result += "MinUpdateRate : " + std::to_string(minUpdateRate) +
          " " + (useMinUpdateRate ? "Active" : "Not Active") + "\n";
  result += "MaxIteration : " + std::to_string(maxIteration) +
          " " + (useMaxIteration ? "Active" : "Not Active") + "\n";
  return result;
}

void Julia::Stop(const RR::InputState& is, sf::Keyboard::Key k) {
    stopped = !stopped;
}

void Julia::SwitchUseMinUpdateRate(const RR::InputState& is, sf::Keyboard::Key k) {
    useMinUpdateRate = !useMinUpdateRate;
}

void Julia::SwitchUseMaxIteration(const RR::InputState& is, sf::Keyboard::Key k) {
    useMaxIteration = !useMaxIteration;
}

std::complex<double> Julia::GetC(sf::Vector2u pixel,
                                 sf::Vector2<double> sample) {
    return cList[indexDisplayed];
}

std::complex<double> Julia::GetZ0(sf::Vector2u pixel,
                                  sf::Vector2<double> sample) {
    return std::complex<double>(rmin + (pixel.x + sample.x) /
                                width * (rmax - rmin),
                                imin + (pixel.y + sample.y) /
                                height * (imax - imin));
}

void Julia::Reset() {
    iteration = 0;
    data.resize(msaa);
    for (int s = 0; s < msaa; ++s) {
        data[s].resize(buffer.getSize().x * buffer.getSize().y);
        ResetLayer(s);
    }
    stopped = false;
}

void Julia::Reset(int msaa) {
    resetMtx.lock();

    this->msaa = msaa;

    Reset();

    resetMtx.unlock();
}

void Julia::ResetLayer(int layer) {
    #pragma omp parallel for
    for (int i = 0; i < buffer.getSize().x * buffer.getSize().y; ++i) {
        sf::Vector2<double> sample(0.0, 0.0);

        if (layer > 0) {
            sample.x = distribSample(generator);
            sample.y = distribSample(generator);
        }

        // Compute each pixel coord from the index
        unsigned int x = i % buffer.getSize().x;
        unsigned int y = (i - x) / buffer.getSize().x;
        data[layer][i].Z = GetZ0(sf::Vector2u(x, y), sample);
        data[layer][i].C = GetC(sf::Vector2u(x, y), sample);
        data[layer][i].Running = true;
        data[layer][i].Iteration = 0;
    }
}

void Julia::UpdateBuffer() {
    resetMtx.lock();
    sf::Clock clock;

    if (!stopped &&
            ((iteration < maxIteration && useMaxIteration)
             || !useMaxIteration)) {
        for (int s = 0; s < msaa; ++s) {
            #pragma omp parallel for
            for (int i = 0; i < buffer.getSize().x * buffer.getSize().y; ++i) {
                if (data[s][i].Running) {
                    ++data[s][i].Iteration;
                    data[s][i].Z = pow(data[s][i].Z, dim) + data[s][i].C;
                    if (std::abs(data[s][i].Z) >= 2.0)
                        data[s][i].Running = false;
                }
            }
        }

        ++iteration;
    }

    #pragma omp parallel for
    for (int i = 0; i < buffer.getSize().x * buffer.getSize().y; ++i) {
        unsigned int x = i % buffer.getSize().x;
        unsigned int y = (i - x) / buffer.getSize().x;

        sf::Vector3i colorSum(0, 0, 0);
        float factorSum = 0.0f;

        for (int s = 0; s < msaa; ++s) {
            if (data[s][i].Iteration < iteration) {
                float factor = static_cast<float>(data[s][i].Iteration) /
                               static_cast<float>(iteration);

                float power = RR::Clamp(static_cast<float>(iteration) / 255.f, 0.f, 1.f);
                power *= 8;
                power += 2;

                factor = 1 - std::pow(1 - factor, power);

                factorSum += factor;

                sf::Color color = cm.GetColor(factor);
                float contrastValue = 0.2f;

                color.r *= std::pow(factor, contrastValue);
                color.g *= std::pow(factor, contrastValue);
                color.b *= std::pow(factor, contrastValue);

                colorSum += ColorToVector(color);
            }
        }

        sf::Color colorFinal = sf::Color::Black;

        colorFinal.r = static_cast<float>(colorSum.x) / static_cast<float>(msaa);
        colorFinal.g = static_cast<float>(colorSum.y) / static_cast<float>(msaa);
        colorFinal.b = static_cast<float>(colorSum.z) / static_cast<float>(msaa);

        buffer.setPixel(x, y, colorFinal);
    }

    int timeMS = clock.getElapsedTime().asMilliseconds();
    float time = clock.getElapsedTime().asSeconds();

    std::cout << "Julia time : "
              << time << "s"
              << std::endl;

    if (timeMS < minUpdateRate && useMinUpdateRate) {
        std::this_thread::sleep_for(
                    std::chrono::milliseconds(minUpdateRate - timeMS));
    }

    resetMtx.unlock();
}

void Julia::IncreaseMSAA(const RR::InputState& is, sf::Keyboard::Key k) {
    Reset(msaa + 1);
}

void Julia::DecreaseMSAA(const RR::InputState& is, sf::Keyboard::Key k) {
    if (msaa > 1) {
        Reset(msaa - 1);
    }
}

void Julia::IncreaseDim(const RR::InputState& is, sf::Keyboard::Key k) {
    ++dim;
    Reset(msaa);
}

void Julia::DecreaseDim(const RR::InputState& is, sf::Keyboard::Key k) {
    if (dim > 2) {
        --dim;
        Reset(msaa);
    }
}

void Julia::VerticalMove(const RR::InputState& is, sf::Keyboard::Key k) {
    double delta = imax - imin;
    delta *= 0.1;
    if (k == sf::Keyboard::Up) {
        imin -= delta;
        imax -= delta;
    } else {
        imin += delta;
        imax += delta;
    }
    Reset(msaa);
}

void Julia::HorizontalMove(const RR::InputState& is, sf::Keyboard::Key k) {
    double delta = rmax - rmin;
    delta *= 0.1;
    if (k == sf::Keyboard::Left) {
        rmin -= delta;
        rmax -= delta;
    } else {
        rmin += delta;
        rmax += delta;
    }
    Reset(msaa);
}

void Julia::Zoom(const RR::InputState& is, sf::Keyboard::Key k) {
    double deltaX = rmax - rmin;
    double deltaY = imax - imin;
    deltaX *= 0.1;
    deltaY *= 0.1;

    if (k == sf::Keyboard::Z) {
        rmin += deltaX;
        rmax -= deltaX;
        imin += deltaY;
        imax -= deltaY;
    } else {
        rmin -= deltaX;
        rmax += deltaX;
        imin -= deltaY;
        imax += deltaY;
    }
    Reset(msaa);
}

void Julia::RandomizeC(const RR::InputState& is, sf::Keyboard::Key k) {
    ++indexDisplayed;
    indexDisplayed %= cList.size();
    std::cout << "C = " << cList[indexDisplayed].real()
              << " + " << cList[indexDisplayed].imag() << "i" << std::endl;

    imin = -1.f;
    imax = 1.f;
    double temp = 3 * width / (height * 1.5);
    temp /= 2;
    rmin = temp * -1;
    rmax = temp;

    Reset(msaa);
}

void Julia::UltraZoom(const RR::InputState& is, sf::Mouse::Button b) {
    double deltaX = rmax - rmin;
    double deltaY = imax - imin;

    double r = static_cast<double>(is.MousePosition.x) /
            static_cast<double>(buffer.getSize().x) * deltaX + rmin;
    double i = static_cast<double>(is.MousePosition.y) /
            static_cast<double>(buffer.getSize().y) * deltaY + imin;

    double zoomFactor = 2.0;

    if (b == sf::Mouse::Button::Left)
        zoomFactor *= 7;

    rmin = r - deltaX / zoomFactor;
    rmax = r + deltaX / zoomFactor;

    imin = i - deltaY / zoomFactor;
    imax = i + deltaY / zoomFactor;

    Reset(msaa);
}

}  // namespace Drawing
