// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_APP_COLORMAPVIEWER_H_

#define SRC_APP_COLORMAPVIEWER_H_

#include <random>
#include <string>
#include <vector>

#include <SFML/Graphics.hpp>

#include "Tools/ColorMap.h"
#include "Window/ImageApp.h"

namespace Drawing {

class Window;

/**
 * @brief An app to view a color map
 */
class ColorMapViewer : public ImageApp {
 public:
    /**
     * @brief Construct an new color map app
     */
    ColorMapViewer(const sf::Vector2u& size, Window* window);

    /// Update the texture
    void UpdateBuffer() override;

    /// Get the help text of the ColorMapViewer app
    std::string GetCommandText() override;

 protected:
    ColorMap cm;
};

}  // namespace Drawing

#endif  // SRC_APP_COLORMAPVIEWER_H_
