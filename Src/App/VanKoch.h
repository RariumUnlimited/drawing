// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_APP_VANKOCH_H_

#define SRC_APP_VANKOCH_H_

#include <vector>
#include <string>

#include <Rarium/Window/InputState.h>
#include <SFML/Graphics.hpp>

#include "Window/VAApp.h"

namespace Drawing {

class Window;

struct Segment {
    sf::Vector2<double> A;
    sf::Vector2<double> B;

    Segment() { }
    Segment(sf::Vector2<double> a, sf::Vector2<double> b) : A(a), B(b) { }
};

/**
 * @brief An app that draw Koch snowflake
 */
class VanKoch : public VAApp {
 public:
    /**
     * @brief Construct an new VanKoch app
     */
    VanKoch(const sf::Vector2u& size, Window* window);

    /// Update the texture
    void UpdateBuffer() override;

    /// Get the help text of the Julia app
    std::string GetCommandText() override;

    /// Get the Info text of the Julia app
    std::string GetInfoText() override;

 protected:
    void IncreaseIteration(const RR::InputState& is, sf::Keyboard::Key k);
    void UltraZoom(const RR::InputState& is, sf::Mouse::Button b);

    void Reset() override;

    double xmin;
    double xmax;
    double ymin;
    double ymax;
    int iteration;
    int width;
    int height;

    int maxIteration;
    bool refresh;

    std::mutex resetMtx;  ///< Mutex used when to avoid data race when resetting

    std::vector<Segment> segments;
};

}  // namespace Drawing

#endif  // SRC_APP_VANKOCH_H_
