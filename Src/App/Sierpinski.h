// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_APP_SIERPINSKI_H_

#define SRC_APP_SIERPINSKI_H_

#include <vector>
#include <random>
#include <string>

#include <Rarium/Window/InputState.h>
#include <SFML/Graphics.hpp>

#include "Window/VAApp.h"

namespace Drawing {

class Window;

/**
 * @brief An app that draw a Sierpinski triangle
 */
class Sierpinski : public VAApp {
 public:
    /**
     * @brief Construct an new Sierpinski app
     */
    Sierpinski(const sf::Vector2u& size, Window* window);

    /// Update the texture
    void UpdateBuffer() override;

    /// Get the help text of the Julia app
    std::string GetCommandText() override;

    /// Get the Info text of the Julia app
    std::string GetInfoText() override;

    void IncreasePointPerUpdate(const RR::InputState& is, sf::Keyboard::Key k);
    void DecreasePointPerUpdate(const RR::InputState& is, sf::Keyboard::Key k);

 protected:
    void Reset() override;

    std::mutex resetMtx;  ///< Mutex used when to avoid data race when resetting

    int pointPerUpdate{ 1 };  ///< Point to compute per update call

    //sf::Vector2f p1;  ///< Bottom left vertex of the triangle
    //sf::Vector2f p2;  ///< Bottom right vertex of the triangle
    //sf::Vector2f p3;  ///< Top vertex of the triangle
    std::vector<sf::Vector2f> triangle;  ///< Triangle ([0] => p1, [1] => p2, [2] => p3)
    sf::Vector2f a;  ///< p2 - p1
    sf::Vector2f b;  ///< p3 - p1

    sf::Vector2f currentPosition;  ///< Current position in the triangle;

    std::mt19937 generator;  ///< A random number generator
    std::uniform_real_distribution<float> distrib;  ///< Distribution for the random point
    std::uniform_int_distribution<int> distribVertex;  ///< Distribution for the random vertex choice

    std::vector<sf::Vector2f> points;
};

}  // namespace Drawing

#endif  // SRC_APP_SIERPINSKI_H_
