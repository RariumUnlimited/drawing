// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_APP_VORONOI_H_

#define SRC_APP_VORONOI_H_

#include <random>
#include <string>
#include <vector>

#include <Rarium/Window/InputState.h>
#include <SFML/Graphics.hpp>

#include "Window/ImageApp.h"

namespace Drawing {

class Window;

/**
 * @brief An app that draw a Voronoi diagram
 */
class Voronoi : public ImageApp {
 public:
    /**
     * @brief Construct an new voronoi app
     */
    Voronoi(const sf::Vector2u& size, Window* window, int pointCount, bool color = true);

    /// Update the texture
    void UpdateBuffer() override;

    /// Get the help text of the Voronoi app
    std::string GetCommandText() override;

    /// Get the Info text of the Voronoi app
    std::string GetInfoText() override;

 protected:
    void RandomizePoints(const RR::InputState& is, sf::Keyboard::Key k);
    void RandomizeColors(const RR::InputState& is, sf::Keyboard::Key k);
    void SwitchColor(const RR::InputState& is, sf::Keyboard::Key k);
    void SwitchPoints(const RR::InputState& is, sf::Keyboard::Key k);
    void AddPoint(const RR::InputState& is, sf::Keyboard::Key k);
    void RemovePoint(const RR::InputState& is, sf::Keyboard::Key k);

    /// Reset the app : set msaa to 1 and pixels to 0
    void Reset() override;

    /**
     * @brief Compute the squared distance between two points
     * @param a A point
     * @param b An another point
     * @return The squared distance between a and b
     */
    float DistanceSquared(const sf::Vector2f& a, const sf::Vector2f& b);

    /**
     * @brief Get the color of a pixel sample
     * @param sample The pixel
     * @return The color of the pixel sample
     */
    sf::Color GetSampleColor(const sf::Vector2f& sample);

    inline sf::Vector3i ColorToVector(const sf::Color& color) {
        return sf::Vector3i(color.r,
                            color.g,
                            color.b);
    }

    std::vector<sf::Vector2f> points;  ///< List of points
    std::vector<sf::Color> colors;  ///< Color of each point
    std::vector<sf::Vector3i> pixels;  ///< Color of each samples of each pixels
    std::mutex resetMtx;  ///< Mutex used when to avoid data race when resetting
    bool color;  ///< True if we have rgb colors, false if we have grey scale
    std::mt19937 generator;  ///< A random number generator
    std::uniform_real_distribution<float> distribX;  ///< Distribution for the point x coordinate
    std::uniform_real_distribution<float> distribY;  ///< Distribution for the point y coordinate
    std::uniform_real_distribution<float> distribSample;  ///< Distribution for the point y coordinate
    std::uniform_int_distribution<unsigned int> distribC;  ///< Distribution the color channels
    bool drawPoints;  ///< If the points are drawn
    int msaa;  ///< Multi sample anti aliasing level
};

}  // namespace Drawing

#endif  // SRC_APP_VORONOI_H_
