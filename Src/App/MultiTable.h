// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_APP_MULTITABLE_H_

#define SRC_APP_MULTITABLE_H_

#include <mutex>
#include <string>

#include <Rarium/Window/InputState.h>
#include <SFML/Graphics.hpp>

#include "Tools/ColorMap.h"
#include "Window/VAApp.h"

namespace Drawing {

class Window;

/**
 * @brief An app that draw a visual representation of multiplication table
 */
class MultiTable : public VAApp {
 public:
    /**
     * @brief Construct an new MultiTable app
     */
    MultiTable(const sf::Vector2u& size, Window* window);

    /// Update the texture
    void UpdateBuffer() override;

    /// Get the help text of the Julia app
    std::string GetCommandText() override;

    /// Get the Info text of the Julia app
    std::string GetInfoText() override;

 protected:
    void SwitchUseMinUpdateRate(const RR::InputState& is, sf::Keyboard::Key k);
    void IncreaseTable(const RR::InputState& is, sf::Keyboard::Key k);
    void DecreaseTable(const RR::InputState& is, sf::Keyboard::Key k);
    void IncreaseMod(const RR::InputState& is, sf::Keyboard::Key k);
    void DecreaseMod(const RR::InputState& is, sf::Keyboard::Key k);

    /// Reset the app : set Z to 0, Running to true and Iteration to 0 for each pixel; and maxIterations to 0
    void Reset();

    int width;
    int height;

    int minUpdateRate;
    bool useMinUpdateRate;

    ColorMap cm;

    int circleVertexCount;

    int table;
    int currentI;
    int mod;

    float r;
    sf::Vector2f center;

    std::mutex resetMtx;  ///< Mutex used when to avoid data race when resetting
};

}  // namespace Drawing

#endif  // SRC_APP_MULTITABLE_H_
