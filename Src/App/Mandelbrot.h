// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_APP_MANDELBROT_H_

#define SRC_APP_MANDELBROT_H_

#include "App/Julia.h"

namespace Drawing {

class Window;

/**
 * @brief An app that draw a Mandelbrot fractal
 */
class Mandelbrot : public Julia {
 public:
    /**
     * @brief Construct an new Mandelbrot app
     */
    Mandelbrot(const sf::Vector2u& size, Window* window);

 protected:
    /// Get the C of a pixel
    std::complex<double> GetC(sf::Vector2u pixel,
                              sf::Vector2<double> sample) override;
    /// Get the Z0 of a pixel
    std::complex<double> GetZ0(sf::Vector2u pixel,
                               sf::Vector2<double> sample) override;
};

}  // namespace Drawing

#endif  // SRC_APP_MANDELBROT_H_
