// Copyright RariumUnlimited - Licence : MIT
#include "App/VanKoch.h"

#include <omp.h>

#include <cfloat>
#include <chrono>
#include <cmath>
#include <iostream>
#include <limits>
#include <string>

#include "Window/Window.h"

#define pi 3.14159265359

namespace Drawing {

VanKoch::VanKoch(const sf::Vector2u& size, Window* window) {
    ymin = -1.f;
    ymax = 1.f;
    width = size.x;
    height = size.y;
    double temp = 3 * width / (height * 1.5);
    temp /= 2;
    xmin = temp * -1;
    xmax = temp;
    iteration = 0;
    maxIteration = 0;

    refresh = true;

    window->KeyPressedEvents[sf::Keyboard::Key::Z] +=
            new RR::KeyEvent::T<VanKoch>(this, &VanKoch::IncreaseIteration);

    window->MouseButtonDoubleClickEvent +=
            new RR::MouseButtonEvent::T<VanKoch>(this, &VanKoch::UltraZoom);
}

std::string VanKoch::GetCommandText() {
    std::string result = "Command : \n";
    result += "Double Left Click : Zoom to cursor \n";
    result += "Z : Increase iteration";
    return result;
}

std::string VanKoch::GetInfoText() {
  std::string result = "Info : \n";
  result += "Iteration : " + std::to_string(iteration) + "\n";
  result += "Segments : " + std::to_string(segments.size()) + "\n";
  result += "MaxIteration : " + std::to_string(maxIteration) + "\n";
  return result;
}

void VanKoch::IncreaseIteration(const RR::InputState& is, sf::Keyboard::Key k) {
    ++maxIteration;
}

void VanKoch::UpdateBuffer() {
    resetMtx.lock();
    sf::Clock clock;

    sf::Rect<double> rect(xmin, ymin, xmax - xmin, ymax - ymin);

    if (iteration < maxIteration) {
        std::vector<Segment> newSegments;

#pragma omp parallel
        {
            std::vector<Segment> newSeg_private;
#pragma omp for
            for (int i = 0; i < segments.size(); ++i) {
                Segment& seg = segments[i];

                sf::Vector2<double> c((2.0 * seg.A.x + seg.B.x) / 3.0, (2.0 * seg.A.y + seg.B.y) / 3.0);
                sf::Vector2<double> d((seg.A.x + 2.0 * seg.B.x) / 3.0, (seg.A.y + 2.0 * seg.B.y) / 3.0);
                sf::Vector2<double> e;
                e.x = c.x + (d.x - c.x) * cos(pi / 3.0) + (d.y - c.y) * sin(pi / 3.0);
                e.y = c.y - (d.x - c.x) * sin(pi / 3.0) + (d.y - c.y) * cos(pi / 3.0);
                if (rect.contains(seg.A) || rect.contains(c))
                    newSeg_private.push_back(Segment(seg.A, c));
                if (rect.contains(c) || rect.contains(e))
                    newSeg_private.push_back(Segment(c, e));
                if (rect.contains(e) || rect.contains(d))
                    newSeg_private.push_back(Segment(e, d));
                if (rect.contains(d) || rect.contains(seg.B))
                    newSeg_private.push_back(Segment(d, seg.B));
            }
#pragma omp critical
            newSegments.insert(newSegments.end(), newSeg_private.begin(), newSeg_private.end());
        }

        segments = newSegments;

        ++iteration;
        refresh = true;
    }

    if (refresh) {
        std::vector<sf::Vertex> vertices;

#pragma omp parallel
        {
            std::vector<sf::Vertex> vertices_private;
#pragma omp for
            for (int i = 0; i < segments.size(); ++i) {
                Segment& seg = segments[i];
                if (rect.contains(seg.A) || rect.contains(seg.B)) {
                    sf::Vector2<double> a = seg.A;
                    sf::Vector2<double> b = seg.B;

                    a.x = ((a.x - xmin) / (xmax - xmin)) * width;
                    a.y = ((a.y - ymin) / (ymax - ymin)) * height;
                    b.x = ((b.x - xmin) / (xmax - xmin)) * width;
                    b.y = ((b.y - ymin) / (ymax - ymin)) * height;

                    sf::Vertex v1(sf::Vector2f(a.x, a.y), sf::Color::White);
                    sf::Vertex v2(sf::Vector2f(b.x, b.y), sf::Color::White);
                    vertices_private.push_back(v1);
                    vertices_private.push_back(v2);
                }
            }
#pragma omp critical
            vertices.insert(vertices.end(), vertices_private.begin(), vertices_private.end());
        }

        buffer = sf::VertexArray(sf::Lines, vertices.size());

        for (unsigned int i = 0; i < vertices.size(); ++i) {
            buffer[i] = vertices[i];
        }

        refresh = false;
    }

    float time = clock.getElapsedTime().asSeconds();

    std::cout << "VanKoch time : "
              << time << "s"
              << std::endl;

    std::this_thread::sleep_for(std::chrono::milliseconds(20));

    resetMtx.unlock();
}


void VanKoch::UltraZoom(const RR::InputState& is, sf::Mouse::Button b) {
    if (b != sf::Mouse::Button::Left)
        return;

    double deltaX = xmax - xmin;
    double deltaY = ymax - ymin;

    double r = static_cast<double>(is.MousePosition.x) /
            static_cast<double>(width) * deltaX + xmin;
    double i = static_cast<double>(is.MousePosition.y) /
            static_cast<double>(height) * deltaY + ymin;

    double zoomFactor = 2.0;

    zoomFactor *= 7;

    xmin = r - deltaX / zoomFactor;
    xmax = r + deltaX / zoomFactor;

    ymin = i - deltaY / zoomFactor;
    ymax = i + deltaY / zoomFactor;

    refresh = true;
}

void VanKoch::Reset() {
    resetMtx.lock();
    iteration = 0;
    segments.clear();
    Segment s;
    s.A = sf::Vector2<double>(-1.0, -0.5);
    s.B = sf::Vector2<double>(1.0, 0.5);
    segments.push_back(s);
    resetMtx.unlock();
}

}  // namespace Drawing
